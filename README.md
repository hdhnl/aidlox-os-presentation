---
Metadata
Title: AidLux OS. Android, AI και Linux μαζί στην ίδια συσκευή.
Alternative Title: AidLux OS. Android, AI & Linux.
Contributor: @nikaskonstantinos
Language: el
Date Issued: 28-01-2023
Publisher: Linux User Forum – Το forum των Ελλήνων χρηστών Linux – https://linux-user.gr/
Description: The future of android with linux. Case study: Aidlux Os
Category: Άρθρα και οδηγοί
Tags:  android, linux, debian, aidlux, deep-learning.
---

![2023-01-28_14-44adlux0|690x316](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-44adlux0.png)
Εικόνα 1. Αναμετάδοση οθόνης κινητού τηλεφώνου με AidLux Os σε φορητό υπολογιστή.

## Εισαγωγή
Πως θα είναι άραγε τα κινητά τηλέφωνα στο μέλλον.
![2023-01-28_14-11adlux1|690x330](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-11adlux1.png)
Εικόνα 2. Αρχική οθόνη aidlux os.

Ίσως, η εφαρμογή AidLux για την εγκατάσταση του ομώνυμου λειτουργικού συστήματος με τη χρήση τεχνητής νοημοσύνης σε android και harmony συσκευές, πιθανόν με proot debian να αποτελεί  μία πρώτη προσπάθεια.
![2023-01-28_14-16adlux2|690x322](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-16adlux2.png)
Εικόνα 3. Επιλογή εμφάνισης από τις ρυθμίσεις.

Η ονομασία της εφαρμογής προέρχεται από τη συνένωση των λέξεων ai, android και linux, γιατί πράγματι το linux φαίνεται ότι θα διαδραματίσει σπουδαίο ρόλο στις μελλοντικές τεχνολογικές εξελίξεις.
![2023-01-28_14-23adlux4|690x324](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-23adlux4.png)
Εικόνα 4. Εξομοιωτής τερματικού.

Η τελευταία έκδοση 1.3.0, μεγέθους 1,2Gb apk στην κινεζική γλώσσα μπορεί να εγκατασταθεί από την ηλεκτρονική διεύθυνση <https://github.com/aidlearning/AidLearning-Framework>

Πρωταρχικός σκοπός της εφαρμογής, είναι η εκμάθηση της μηχανικής μάθησης μέσα από παραδείγματα με κατάλληλες εφαρμογές εγκατεστημένες σε μία συσκευή smartphone ή tablet, ώς σταθμό λειτουργίας και πρόσβασης από τον υπολογιστή μας ή από την ίδια τη συσκευή.

## Εγκατάσταση
Η εγκατάσταση είναι δύσκολή μόνο όσον αφορά τη γλώσσα ή οποία είναι αποκλειστικά η κινεζική προς το παρόν.
Αφού κάνουμε λήψη του αρχείου apk και εγκατάσταση στο κινητό τηλέφωνό μας, έπειτα κάνουμε εγγραφή με το email   μας ή τον τηλ. αριθμό μας και αφού λύσουμε ένα παζλ και ενεργοποιήσουμε τις ``επιλογές για προγραμματιστές`` στο κινητό μας, προχωράμε στη γραμμή εντολών για την εγκατάσταση του περιβάλλοντος εργασίας με την εντολή που προτείνεται:
``aid install aid-desktop``


## Χρήση
Η πλοήγηση στο περιβάλλον εργασίας είναι πολύ ευχάριστη και με άμεση ανταπόκριση.
Στο κέντρο λογισμικού το οποίο είναι διττό   linux  από τη μια και android  από την άλλη θα βρούμε εφαρμογές(πακέτα) της aidlux  ή της debian έτοιμες για εγκατάσταση μέσα από τη γραφική διεπαφή του χρήστη και τις ήδη εγκατεστημένες εφαρμογές του κινητού μας τηλεφώνου.
![2023-01-28_14-27adlux5|690x327](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-27adlux5.png)
Εικόνα 5. Κέντρο εγκατάστασης λογισμικού.

Το περιβάλλον περιγράφεται ως aid-desktop με πολύ αναβαθμισμένη αισθητική
, ενώ υπάρχει εγκατεστημένο το xfce και επίσης προφέρεται το lxde στο κέντρο λογισμικού.

![2023-01-28_14-32adlux7|690x332](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-32adlux7.png)
Εικόνα 6. Διαχειριστής αρχείων.

Ο φυλλομετρητής είναι ο baidu.



Και τέλος υπάρχουν έτοιμα παραδείγματα deep learning, για εγκατάσταση με σκοπό την εκμάθηση του χρήστη.
![2023-01-28_14-33adlux8|690x329](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_14-33adlux8.png)
Εικόνα 7. Εκμάθηση με παραδείγματα   deep-learning.

## Εν κατακλείδι
Συνοψίζοντας, το aidlux os έναι μία πρόγευση του μέλλοντος προερχόμενη από την Κίνα, με άψογη αισθητική, λειτουργικότητα και διαδραστικότητα για την εκπαίδευση σε νέες τεχνολογίες με τη βοήθεια του linux.

![2023-01-28_15-46adlux9|690x322](https://gitlab.com/hdhnl/aidlox-os-presentation/-/raw/main/images/2023-01-28_15-46adlux9.png)
Εικόνα 8. Aidlux OS.

Πηγές:
1. <https://github.com/aidlearning/AidLearning-Framework>
2. <https://gitlab.com/hdhnl/aidlox-os-presentation>

